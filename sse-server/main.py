from fastapi import FastAPI, Request,WebSocket,WebSocketDisconnect
from websockets.exceptions import ConnectionClosed
from fastapi.responses import StreamingResponse
from fastapi.responses import HTMLResponse
from notification import send_notification
from utils import streamed_events
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from sse_starlette.sse import EventSourceResponse
import asyncio


templates = Jinja2Templates(directory="templates")


app = FastAPI(title="SSE-Server")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class EventInModel(BaseModel):
    event:str 
    subject:str
    message:str


class WebsocketManager:
    def __init__(self):
        self.__clients: list[WebSocket] = []
    
    @property
    def clients(self) -> list[WebSocket]:
        return self.__clients
    
    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.__clients.append(websocket)
    async def disconnect(self, websocket: WebSocket):
        await websocket.close()
        self.__clients.remove(websocket)

    async def broadcast(self, data: dict):
        for client in self.__clients:
            try:
                await client.send_json(data)
            except (WebSocketDisconnect,ConnectionClosed):
                await self.disconnect(client)

manager = WebsocketManager()

@app.post("/make")
async def send_event(payload:EventInModel):
   kwargs = payload.model_dump()
   send_notification(**kwargs)


@app.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id:int):
    await manager.connect(websocket)
    while websocket.client_state.CONNECTED:
        try:
            async for text in streamed_events('test'):
                await manager.broadcast(text)
        except (WebSocketDisconnect, ConnectionClosed):
            await manager.disconnect(websocket)        
            break