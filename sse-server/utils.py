from fastapi import Request
from typing import AsyncGenerator
from notification import get_async_client

import asyncio
import json


async def streamed_events(event_name:str) -> AsyncGenerator:
    try:
        async with get_async_client().pubsub() as pubsub:
            await pubsub.subscribe(event_name)
            while True:
                msg = await pubsub.get_message(ignore_subscribe_messages=True, timeout=None)
                if msg is None:
                    continue
                data = json.loads(msg["data"])
                yield data
    except asyncio.CancelledError:
        # Do any cleanup when the client disconnects
        # Note: this will only be called starting from Django 5.0; until then, there is no cleanup,
        # and you get some spammy 'took too long to shut down and was killed' log messages from Daphne etc.
        raise