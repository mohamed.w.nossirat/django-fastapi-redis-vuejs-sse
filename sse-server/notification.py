import json
from functools import cache
import redis
import redis.asyncio as aredis

@cache
def get_async_client() -> aredis.Redis:
    return aredis.from_url("redis://redis:6379")

@cache
def get_client() -> redis.Redis:
    return redis.from_url("redis://redis:6379")

def send_notification(event:str, **kwargs):
    data = json.dumps(kwargs)
    get_client().publish(event,data )