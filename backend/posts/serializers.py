from rest_framework import serializers
from .models import Post
import requests
import json

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

    def create(self, validated_data):
        requests.post("http://sse-server:5000/make/",json.dumps({'event':'test','subject':validated_data['title'], 'message':validated_data['body']}) )
        return super().create(validated_data)