from rest_framework import routers
from . import views

router = routers.DefaultRouter()

router.register(prefix="posts", viewset=views.PostViewSet, basename="post")

urlpatterns = router.urls